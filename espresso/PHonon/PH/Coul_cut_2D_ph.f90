!
! Copyright (C) 2007-2011 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
! 
! TS new module for all the variables and subroutines necessary to cutoff
!----------------------------------------------------------------------------
MODULE Coul_cut_2D_ph
  !----------------------------------------------------------------------------
  !
  ! ... this module contains the variables and subroutines needed for the 
  ! ... 2D Coulomb cutoff technique 
  !
  USE kinds, ONLY :  DP
  USE constants, ONLY : tpi, pi
  SAVE
  !
  REAL(DP), ALLOCATABLE :: cutoff_2D_qg(:)
  COMPLEX(DP), ALLOCATABLE :: lr_Vlocq(:,:)
  ! 
  !
  PUBLIC :: cutoff_fact_qg, cutoff_lr_Vlocq, cutoff_localq
  
CONTAINS
!
! TS new subroutine to define cutoff factor in at q+G
!----------------------------------------------------------------------
subroutine cutoff_fact_qg ()
  !----------------------------------------------------------------------
  !
  !
  USE kinds
  USE io_global, ONLY : stdout
  USE gvect,     ONLY : g, ngm
  USE cell_base, ONLY : alat, celldm, at
  USE qpoint,    ONLY : xq 
  implicit none
  !
  !
  integer :: ng, i
  ! counter over G vectors
  real(DP) :: qGzlz, qGplz, lz
  !
  !WRITE(stdout, *) "thibault : Calculating cutoff factorat q+G in ph, with ngm= ", ngm
  IF (.not.ALLOCATED(cutoff_2D_qg)) ALLOCATE( cutoff_2D_qg (ngm) )  
  ! lz=celldm(3)/2.0d0*alat
  ! This does not work for every bravais lattice. 
  ! Only the ones such that
  ! v3=(0,0,celldm(3)/a)
  do i=1,2
     IF (abs(at(3,i))>1d-8) WRITE(stdout, *) "2D CODE WILL NOT WORK, 2D MATERIAL NOT IN X-Y PLANE!!"
  enddo
  lz=0.5d0*at(3,3)*alat
  do ng = 1, ngm
     qGplz=SQRT( ( xq(1)+ g (1, ng) )**2 + &
         ( xq(2)+ g (2, ng) )**2 )*tpi*lz/alat
     qGzlz= ( xq(3)+ g (3, ng))*tpi*lz/alat
     ! sufficient if xq3=0
     cutoff_2D_qg(ng)= 1.0d0- exp(-qGplz)*cos(qGzlz)
     !cutoff_2D_qg(ng)= 1.0d0+ exp(-qGplz)* &
     !        (qGzlz/qGplz*sin(qGzlz) -cos(qGzlz))
  enddo
  return
end subroutine cutoff_fact_qg
!
! TS new subroutine to define cutoff long range part of vlocq
!----------------------------------------------------------------------
subroutine cutoff_lr_Vlocq ( )
  !----------------------------------------------------------------------
  !
  !   calculate the long-range part of vlocq(g) cutoff for 2D calculations
  !
  USE kinds
  USE constants,   ONLY : fpi, e2, eps8
  USE gvect,      ONLY :  g, ngm
  !USE gvecs,      ONLY : ngms
  USE ions_base,  ONLY : zv, nsp
  USE uspp_param, ONLY : upf
  USE cell_base,  ONLY : omega, tpiba2
  USE qpoint,    ONLY : xq
  USE io_global, ONLY: stdout
  implicit none
  ! local variable
  integer :: ig, nt !, ngstart
  REAL(DP)::fac, g2a
  ! counter over G vectors
  !WRITE(stdout, *) "thibault : Calculating cutoff lr vlocq in ph"
  IF (.not.ALLOCATED(lr_Vlocq)) ALLOCATE( lr_Vlocq (ngm,nsp) )
  lr_Vlocq (:,:)=0.0d0
  DO nt = 1, nsp
     fac= upf(nt)%zp * e2 / tpiba2
     DO ig = 1, ngm
        g2a = (xq (1) + g (1, ig) ) **2 + (xq (2) + g (2, ig) ) **2 + &
                (xq (3) + g (3, ig) ) **2  
        if (g2a<eps8) then 
           lr_Vlocq (ig, nt)=0.0d0
        else
           lr_Vlocq (ig, nt)= - fpi / omega* fac * cutoff_2D_qg(ig)* &
                    exp ( - g2a * tpiba2 * 0.25d0) / g2a
        endif 
     END DO
  END DO
  return
end subroutine cutoff_lr_Vlocq
!
! TS new subroutine to re-add long-range part to vlocq
!----------------------------------------------------------------------
subroutine cutoff_localq (dvlocin, fact,  u1, u2, u3, gu0, nt, na)
  !--------------------------------------------------------------------
  !
  USE kinds
  USE io_global, ONLY : stdout
  USE fft_base,  ONLY : dffts
  USE gvect,     ONLY : eigts1, eigts2, eigts3, mill, g
  USE gvecs,   ONLY : ngms, nls
  implicit none
  !
  complex(DP), INTENT(INOUT) :: dvlocin (dffts%nnr)
  COMPLEX(DP), INTENT(IN) :: fact,  u1, u2, u3, gu0
  INTEGER, INTENT(IN)     :: nt, na
  !
  integer :: ig
  complex(DP) :: gtau, gu
  !
  !WRITE(stdout, *) "thibault : re-adding long range part to vlocq in ph"
  do ig = 1, ngms
     gtau = eigts1 (mill(1,ig), na) * eigts2 (mill(2,ig), na) * &
              eigts3 (mill(3,ig), na)
     gu = gu0 + g (1, ig) * u1 + g (2, ig) * u2 + g (3, ig) * u3
     dvlocin (nls (ig) ) = dvlocin (nls (ig) ) + lr_Vlocq (ig, nt) &
              * gu * fact * gtau
  enddo  
  return
end subroutine cutoff_localq
!
! TS new subroutine to cutoff V_Hartree
!----------------------------------------------------------------------
subroutine cutoff_dv_of_drho (dvaux, is, dvscf)
  !----------------------------------------------------------------------
  !
  USE kinds
  USE constants,        ONLY : fpi, e2
  USE cell_base,        ONLY : tpiba2
  USE io_global,        ONLY : stdout
  USE fft_base,         ONLY: dfftp
  USE noncollin_module, ONLY : nspin_mag
  USE gvect,            ONLY : g, ngm, nl
  USE qpoint,           ONLY : xq 
  implicit none
  !
  COMPLEX(DP), INTENT(INOUT) :: dvaux( dfftp%nnr,  nspin_mag)
  COMPLEX(DP), INTENT(IN) :: dvscf (dfftp%nnr, nspin_mag)
  INTEGER, INTENT(IN)     :: is
  !
  INTEGER :: ig
  REAL(DP) :: qg2
  !
  !WRITE(stdout, *) "thibault : cutting off dv_of_drho"
  do ig = 1, ngm
     qg2 = (g(1,ig)+xq(1))**2 + (g(2,ig)+xq(2))**2 + (g(3,ig)+xq(3))**2
     if (qg2 > 1.d-8) then
        dvaux(nl(ig),is) = dvaux(nl(ig),is) + cutoff_2D_qg(ig)*&
                           e2 * fpi * dvscf(nl(ig),1) / (tpiba2 * qg2)
     endif
  enddo
  return
end subroutine cutoff_dv_of_drho
!
! TS new subroutine to cutoff d^2Vloc/dudu * rho^0 
! (1rst part of dynamical matrix)
!----------------------------------------------------------------------
subroutine cutoff_dynmat0 (dynwrk, rhog)
  !----------------------------------------------------------------------
  !
  USE kinds
  USE io_global,   ONLY : stdout
  USE constants,   ONLY : tpi, eps8
  USE cell_base,   ONLY : omega, tpiba2
  USE fft_base,    ONLY: dfftp
  USE gvect,       ONLY : g, ngm, nl, gg
  USE Coul_cut_2D, ONLY : lr_Vloc
  USE ions_base,   ONLY : nat, ityp, ntyp => nsp, tau
  implicit none
  !
  COMPLEX(DP), INTENT(INOUT) :: dynwrk (3 * nat, 3 * nat)
  COMPLEX(DP), INTENT(IN) :: rhog  ( dfftp%nnr)
  !
  integer :: ng, na, icart, jcart, na_icart, na_jcart
  REAL(DP) :: gtau, fac
  !
  DO na = 1, nat
     DO icart = 1, 3
        na_icart = 3 * (na - 1) + icart
        DO jcart = 1, 3
           na_jcart = 3 * (na - 1) + jcart
           DO ng = 1, ngm
              gtau = tpi * (g (1, ng) * tau (1, na) + &
                            g (2, ng) * tau (2, na) + &
                            g (3, ng) * tau (3, na) )
              fac = omega * lr_Vloc ( ng , ityp (na) ) * tpiba2 * &
                   ( DBLE (rhog (nl (ng) ) ) * COS (gtau) - &
                    AIMAG (rhog (nl (ng) ) ) * SIN (gtau) )
                  dynwrk (na_icart, na_jcart) = dynwrk (na_icart, na_jcart) - &
                   fac * g (icart, ng) * g (jcart, ng)
           ENDDO
        ENDDO
     ENDDO
  ENDDO  
  return
end subroutine cutoff_dynmat0
END MODULE Coul_cut_2D_ph